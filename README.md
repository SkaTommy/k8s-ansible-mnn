# Kubernetes (K8s)

[![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/569/badge)](https://bestpractices.coreinfrastructure.org/projects/569) [![Go Report Card](https://goreportcard.com/badge/github.com/kubernetes/kubernetes)](https://goreportcard.com/report/github.com/kubernetes/kubernetes) ![GitHub release (latest SemVer)](https://img.shields.io/github/v/release/kubernetes/kubernetes)

<img src="https://github.com/kubernetes/kubernetes/raw/master/logo/logo.png" width="100">

----

### Create vm with terraform on proxmox

[terraform repo](https://gitlab.com/SkaTommy/terraform-cloudinit)

### Ansible

#### playbooks adapted for Debian family dist

- add ssh key on hosts
- change ip in hosts.ini

#### Start ansible playbook

```bash
ansible-playbook -i hosts.ini main.yml
```